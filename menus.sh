menu_upgrade() {
	clear
	echo -e "\nMENU DE UPGRADE\n"
	echo -e "[1] - Ver informacoes atuais;"
	echo -e "[2] - Mudar informacoes;"
	echo -e "[3] - Preparar ambiente v11;"
	echo -e "[4] - Iniciar Upgrade;"
	echo -e "\n[0] - Voltar"

	read op_upgrade
		if [[ "$op_upgrade"   == 1 ]]
			then
			obter_info
		elif [[ "$op_upgrade" == 2 ]] 
			then
			inserir_dados
		elif [[ "$op_upgrade" == 3 ]] 
			then
			instalar
		elif [[ "$op_upgrade" == 4 ]] 
			then
			upgrade
		elif [[ "$op_upgrade" == 0 ]] 
			then
			menu_inicial
		else
			menu_upgrade
		fi

	echo ""
	read -p "Deseja voltar ao menu? (s/n) " voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

menu_log() {
	clear
	echo -e "\nRELATORIO\n"
	echo -e "[1] - Ver informacoes sobre arquivos;"
	echo -e "[2] - Ver informacoes sobre banco de dados;"
	echo -e "[3] - Ver relatorio completo;"
	echo -e "\n[0] - Voltar"

	read op_log
		if [[ "$op_log" ==  1 ]]
			then
			ver_log
		elif [[ "$op_log" == 0 ]] 
			then
			menu_inicial
		else
			menu_log
		fi

	echo ""
	read -p "Deseja voltar ao menu? (s/n) " voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

menu_bkp() {
	clear
	echo -e "\nMenu de Backup\n"
	echo -e "[1] - Fazer Backup dos Bancos de Dados"
	echo -e "[2] - Fazer Backup dos arquivos da v10.4"
	echo -e "\n[0] - Voltar"


	read op_menu_bkp
	if [[ "$op_menu_bkp"   == 1 ]]
		then
		bkp_bds
	elif [[ "$op_menu_bkp"   == 2 ]]
		then
		bkp_files
	fi
}

menu_inicial() {
	clear
	echo -e "\n\tUTILITARIO V11\n"

	echo -e "[1] - Bloquear Site Manager"
	echo -e "[2] - Desloquear Site Manager\n"

	echo -e "[3] - Ver informacoes em vigor"			
	echo -e "[4] - Mudar informacoes\n"

	echo -e "[5] - Fazer Backup dos Bancos de Dados"	#Precida de info do MySQL
	echo -e "[6] - Fazer Backup do Projeto\n"				

	echo -e "[7] - Preparar ambiente v11"				#Colocar flags 1 ou 0
	echo -e "[8] - Iniciar Upgrade\n"					#Checar se esta preparado

	echo -e "[9] - Relatorio"
	
	echo -e "\n[0] - Sair"

	read op_menu_inicial
	if [[ "$op_menu_inicial"   == 1 ]]
		then
		block_sitemgr
	elif [[ "$op_menu_inicial" == 2 ]]
		then
		unblock_sitemgr
	elif [[ "$op_menu_inicial" == 3 ]] 
		then
		obter_info
	elif [[ "$op_menu_inicial" == 4 ]] 
		then
		inserir_dados
	elif [[ "$op_menu_inicial" == 5 ]] 
		then
		  bkp_bds
	elif [[ "$op_menu_inicial" == 6 ]] 
		then
		  bkp_files
	elif [[ "$op_menu_inicial" == 7 ]] 
		then
		  instalar
	elif [[ "$op_menu_inicial" == 8 ]] 
		then
		  upgrade
	elif [[ "$op_menu_inicial" == 9 ]] 
		then
		  ver_log
	elif [[ "$op_menu_inicial" == 0 ]] 
		then
		exit
	fi
}