#!/bin/bash
##################################################################
##
## Este programa tem por objetivo auxiliar o colaborador durante 
## o processo de upgrade de um eDirectory v10.4 para v11
##
## No momento o intuito do programa e rodar no ecor, entao antes de 
## tentar rodar em algum outro servidor faça uma revisao do codigo
##
## Rafael Carreiro
## Março 2016
##
## Sinta-se a vontade para melhorar o codigo!!!
## rafael.oliveira@arcasolutions.com
###################################################################

cd $HOME/public_html/

p_upgrade=$1
if [[ "$p_upgrade" = "upgrade" ]];then
	cd /opt/upgrade_edirv11/
	sudo git pull
	echo -e "\n[!] Atualizacao Completa!"
	exit 0
fi

#Funcao utilizada para iniciar o programa
main() {

	clear
	echo -e "\tBem-vindo ao UTILITARIO v11!\n"
	echo -e "\nEste e o diretorio root do eDirectory?" 
	echo -e "\n\t >>> `pwd` <<< \n"
	echo  -e "(s/n)  "

	read dir_edir
	if [[ "$dir_edir" == "s" ]]
		then
		#Consertar essa confusao aqui, isso tudo pode ficar em uma unica instrucao
		EDIR_ROOT=`pwd`
		cd .. ; BEFORE_EDIR_ROOT=`pwd` ; mkdir edirectory ; cd edirectory ; EDIR11_ROOT=`pwd`
		cd $EDIR_ROOT

		LOGUP="$BEFORE_EDIR_ROOT/log_upgrade"
		BKP_DIR=$BEFORE_EDIR_ROOT/`date +%d%m%Y`
		menu_inicial
	else
		echo -e "Onde esta localizado a raiz do edirectory?"
		read EDIR_ROOT
		BEFORE_EDIR_ROOT=$(cd .. ; pwd)
		EDIR11_ROOT=$BEFORE_EDIR_ROOT/edirectory
		LOGUP="$BEFORE_EDIR_ROOT/log_upgrade"
		BKP_DIR=$BEFORE_EDIR_ROOT/`date +%d%m%Y`
		menu_inicial
	fi		
}

#####################################################################
#	Operaçoes com sitemgr
#####################################################################
#Bloqueia o SiteManager
block_sitemgr() {
	cd $EDIR_ROOT
	mkdir tmp_upgrade ; cd tmp_upgrade
	wget -bqc "https://s3-sa-east-1.amazonaws.com/upgradev11/base/block_sitemgr.tar.gz"
	tar zxf block_sitemgr.tar.gz
	sed -i.bkp -e '/RewriteEngine\ On/r text-to-htaccess' $EDIR_ROOT/.htaccess
	mv indexUpgrade.html $EDIR_ROOT
	echo -e "`date`\t !!! SITEMGR BLOQUEADO !!! " >> $LOGUP
	echo -e "\n[!]Site Manager bloqueado com sucesso!\n"
	sitemgr_status=1
	msg_sitemgr
	cd -
	menu_inicial
}

#Desbloqueia o SiteManager
unblock_sitemgr() {
	rm -rf $EDIR_ROOT/.htaccess
	mv $EDIR_ROOT/.htaccess.bkp $EDIR_ROOT/.htaccess
	rm -rf $EDIR_ROOT/tmp_upgrade
	rm -rf $EDIR_ROOT/indexUpgrade.html
	echo -e "`date`\t !!! SITEMGR DESBLOQUEADO !!! " >> $LOGUP
	echo -e "\n[!]Site Manager desbloqueado com sucesso!\n"
	sitemgr_status=0
	msg_sitemgr
	menu_inicial
}


#####################################################################
#	Operaçoes
#####################################################################
#Informaçoes do MySQL
inserir_dados_mysql(){
	read -p "Informe o usuario do banco.............: " DB_USER
	read -p "Informe a senha do banco...............: " DB_PASS
	read -p "Informe o nome do banco main...........: " DB_MAIN_NAME
	read -p "Informe o nome do banco domain.........: " DB_DOMAIN1
}

inserir_dados_proj(){
	read -p "Informe o nome do projeto/user ........: " PROJNAME
	read -p "Informe o dominio do projeto (sem www).: " PROJDOMAIN1
	read -p "Informe o IP do HOSTDB (localhost).....: " IP_DB_HOST
}

#Inserir informacoes sobre o projeto
inserir_dados() {
	echo -e "Deseja inserir as informacoes na v10.4 ou v11? (10.4 | 11.0)" 
	read op_inserir_dados

	if [[ "$op_inserir_dados" = "10.4" ]]; then
		cd $EDIR_ROOT/conf/
		sed -i 's/\"_DIRECTORYDB_HOST\"\,\ \ \".*/\"_DIRECTORYDB_HOST\"\,\ \ \"'"$IP_DB_HOST"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_USER\"\,\ \ \".*/\"_DIRECTORYDB_USER\"\,\ \ \"'"$DB_USER"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_PASS\"\,\ \ \".*/\"_DIRECTORYDB_PASS\"\,\ \ \"'"$DB_PASS"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_NAME\"\,\ \ \".*/\"_DIRECTORYDB_NAME\"\,\ \ \"'"$DB_MAIN_NAME"'\"\)\;/' config.inc.php 
		sed -i 's/\"DB_NAME_PREFIX\"\,\ \".*/\"DB_NAME_PREFIX\"\,\ \"'"$PROJNAME"'\"\)\;/' config.inc.php
		cd $EDIR_ROOT/custom/domain
		sed -i "s/\$domainInfo\[\".*/\$domainInfo\[\"$PROJDOMAIN1\"\]\ \=\ 1\;/g" domain.inc.php

			read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi


	elif [[ "$op_inserir_dados" = "11.0" ]] ;then

		inserir_dados_proj
		inserir_dados_mysql

		##### Criar arquivos .yml em "app/config/domains" #####
		echo -e "\n[+]Criando arquivos .yml (configs, payment, route) em app/config/domains"
		cd $EDIR11_ROOT/app/config/domains
		cp domain.configs.yml.sample $PROJDOMAIN1.configs.yml
		cp domain.payment.yml.sample $PROJDOMAIN1.payment.yml
		cp domain.route.yml.sample $PROJDOMAIN1.route.yml

		echo -e "[+]OK\n"
		ls -lah | grep $PROJDOMAIN1* >> $LOGUP
		echo "-------------------------------------------------------------" >> $LOGUP

		##### Editar arquivo "app/config/domain.yml" #####
		 ### Buscar e substituir domínio e nome do projeto ###
		echo "Editando arquivo app/config/domain.yml ..."
		cd ..
		sed -i 's/edirbase.com/'"$PROJDOMAIN1"'/g' domain.yml
		sed -i 's/edirbase/'"$PROJNAME"'/g' domain.yml
		 ### Selecionar Branded ###
		echo "Branded [on/off]:"
		read BRANDED

		while :
		do
		        if [ "$BRANDED" = "on" ]
		        then
		                sed -i 's/.*branded.*/            branded: on/' domain.yml
				break
		        elif [ "$BRANDED" = off ]; then 
		                sed -i 's/.*branded.*/            branded: off/' domain.yml
				break
		        else
		                echo "Resposta invalida!"
				break
			fi
		done

		echo -e "[+]OK\n"
		cat domain.yml  >> $LOGUP
		echo "-------------------------------------------------------------" >> $LOGUP

		##### Editar arquivo "/app/conf/parameters.yml" #####
		echo "Editando arquivo /app/conf/parameters.yml ..."
		sed -i 's/database_name:.*/database_name:\ '"$DB_MAIN_NAME"'/' parameters.yml
		sed -i 's/database_user:.*/database_user:\ '"$DB_USER"'/' parameters.yml
		sed -i 's/database_password:.*/database_password:\ \"'"$DB_PASS"'\"/' parameters.yml

		echo -e "[+]OK\n"
		cat parameters.yml >> $LOGUP
		echo "-------------------------------------------------------------" >> $LOGUP

		##### Editar arquivo "/web/conf/config.inc.php" #####
		echo "Editando arquivo /web/conf/config.inc.php ..."
		cd ../../web/conf/
		sed -i 's/\"_DIRECTORYDB_HOST\"\,\ \ \".*/\"_DIRECTORYDB_HOST\"\,\ \ \"'"$IP_DB_HOST"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_USER\"\,\ \ \".*/\"_DIRECTORYDB_USER\"\,\ \ \"'"$DB_USER"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_PASS\"\,\ \ \".*/\"_DIRECTORYDB_PASS\"\,\ \ \"'"$DB_PASS"'\"\)\;/' config.inc.php
		sed -i 's/\"_DIRECTORYDB_NAME\"\,\ \ \".*/\"_DIRECTORYDB_NAME\"\,\ \ \"'"$DB_MAIN_NAME"'\"\)\;/' config.inc.php 
		sed -i 's/\"DB_NAME_PREFIX\"\,\ \".*/\"DB_NAME_PREFIX\"\,\ \"'"$PROJNAME"'\"\)\;/' config.inc.php

		echo -e "[+]OK\n"
		cat config.inc.php  >> $LOGUP
		echo "-------------------------------------------------------------" >> $LOGUP

		##### Editar o arquivo "web/custom/domain/domain.inc.php" #####
		echo "Editando arquivo web/custom/domain/domain.inc.php ..."
		cd ../custom/domain
		sed -i "s/\$domainInfo\[\".*/\$domainInfo\[\"$PROJDOMAIN1\"\]\ \=\ 1\;/g" domain.inc.php

		echo -e "[+]OK\n"
		cat domain.inc.php >> $LOGUP
		echo "-------------------------------------------------------------" >> $LOGUP

		mysql -u$DB_USER -p$DB_PASS -e "show databases;"
		mysql -u$DB_USER -p$DB_PASS -e "use $DB_MAIN_NAME ; select id,name,database_username,database_password,database_name,url from Domain;"

		echo -e "[+]Sucesso!\n"
		echo "-------------------------------------------------------------" >> $LOGUP

		echo -e "\n[+]Informacoes inseridas com sucesso!"
			read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi


	else 
		inserir_dados
	fi
}



#Baixa o pacote base do eDirectory v11
baixar_fonte() {
	read -p "Deseja baixar o pacote base? (s/n)" baixar_pacote
	if [[ "$baixar_pacote" == "s" ]]
		then
		cd $BEFORE_EDIR_ROOT
		wget -c "https://s3-sa-east-1.amazonaws.com/upgradev11/base/edir11base.tar.gz"
		tar zxf edir11base.tar.gz
		echo -e "\n[+]Diretorio edirectory pronto para ser configurado!\n"
	fi
}

#Obter informacoes ja configuradas no projeto, tanto da v10.4 quanto da v11
obter_info() {
	clear 
	echo -e "Deseja obter informacoes de qual versao? (10.4 | 11.0)" 
	read versao

	if [[ "$versao" == "10.4" ]]
		then
		echo -e "`date` \n" > $LOGUP
		cat $EDIR_ROOT/conf/constants.inc.php | grep 'define("VERSION",' >> $LOGUP 				###Pegar essas informaçoes e jogar para variavel automaticamente
		cat $EDIR_ROOT/custom/domain/domain.inc.php | grep  '\$domainInfo\[\"' >> $LOGUP 		
		ls  $EDIR_ROOT/custom | grep "domain_$*" >> $LOGUP 										
		cat $EDIR_ROOT/conf/config.inc.php | grep 'define("_DIRECTORYDB_HOST",' >> $LOGUP 		
		cat $EDIR_ROOT/conf/config.inc.php | grep 'define("_DIRECTORYDB_USER",' >> $LOGUP 		
		cat $EDIR_ROOT/conf/config.inc.php | grep 'define("_DIRECTORYDB_PASS",' >> $LOGUP 		
		cat $EDIR_ROOT/conf/config.inc.php | grep 'define("_DIRECTORYDB_NAME",' >> $LOGUP 		
		cat $EDIR_ROOT/conf/config.inc.php | grep 'define("DB_NAME_PREFIX"' >> $LOGUP 		
		cat $EDIR_ROOT/custom/domain_1/conf/constants.inc.php | grep -e BRANDED >> $LOGUP			
		sed 's/^[ \t]*/        /' $LOGUP

		echo -e "\nEntre com os as informacoes do BD: "
		read -p "Informe o usuario do banco: " DB_USER
		read -p "Informe a senha do banco: " DB_PASS
		read -p "Informe o nome do banco main (projeto_main): " DB_MAIN_NAME
		mysql -u$DB_USER -p$DB_PASS -e "show databases;"
		mysql -u$DB_USER -p$DB_PASS -e "use $DB_MAIN_NAME ; select id,name,database_username,database_password,database_name,url from Domain;"
		
		echo -e "\n[+]Informacoes inseridas com sucesso!"
		echo ""
			read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi

	elif [[ "$versao" == "11.0" ]]
		then
		echo -e "`date` \n" > $LOGUP
		cat $EDIR11_ROOT/web/custom/domain/domain.inc.php | grep  '\$domainInfo\[\"' >> $LOGUP
		ls  $EDIR11_ROOT/web/custom | grep "domain_$*" >> $LOGUP
		cat $EDIR11_ROOT/web/conf/config.inc.php | grep 'define("_DIRECTORYDB_HOST",' >> $LOGUP
		cat $EDIR11_ROOT/web/conf/config.inc.php | grep 'define("_DIRECTORYDB_USER",' >> $LOGUP
		cat $EDIR11_ROOT/web/conf/config.inc.php | grep 'define("_DIRECTORYDB_PASS",' >> $LOGUP
		cat $EDIR11_ROOT/web/conf/config.inc.php | grep 'define("_DIRECTORYDB_NAME",' >> $LOGUP
		cat $EDIR11_ROOT/web/conf/config.inc.php | grep 'define("DB_NAME_PREFIX"' >> $LOGUP
		cat $EDIR11_ROOT/web/custom/domain_1/conf/constants.inc.php | grep -e BRANDED >> $LOGUP
		cat $EDIR11_ROOT/web/custom/domain_1/conf/constants.inc.php | grep -e MOBILE >> $LOGUP
		sed 's/^[ \t]*/        /' $LOGUP

		echo -e "\nEntre com os as informacoes do BD: "
		read -p "Informe o usuario do banco: " DB_USER
		read -p "Informe a senha do banco: " DB_PASS
		read -p "Informe o nome do banco main (projeto_main): " DB_MAIN_NAME

		mysql -u$DB_USER -p$DB_PASS -e "show databases;"
		mysql -u$DB_USER -p$DB_PASS -e "use $DB_MAIN_NAME ; select id,name,database_username,database_password,database_name,url from Domain;"

		echo ""
		read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi

	else
		echo "Opcao invalida!"
		obter_info

	fi
}

#Fazer backup dos bancos de dados
bkp_bds() {
	inserir_dados
	echo -e "[+]Exportando bancos main e domain1"
	mysqldump -C -u$DB_USER -p$DB_PASS $DB_MAIN_NAME > bkp_$DB_MAIN_NAME.sql
    mysqldump -C -u$DB_USER -p$DB_PASS $DB_DOMAIN1 > bkp_$DB_DOMAIN1.sql

    echo -e "\n[!]Bancos exportados com sucesso!"
    ls -lah | grep "bkp_*"
    read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

#Fazer backup dos arquivos da versao 10.4
bkp_files() {
	clear
	echo -e "Copiando pasta public_html para $BKP_DIR..."
	cd $EDIR_ROOT
		tar zcf bkp_public_html.tar.gz *
		mv bkp_public_html.tar.gz $BKP_DIR
	echo -e "\nBackup concluido!\n"

	read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

#Preparar ambiente para a versao 11, configurando os arquivos necessarios
instalar() {
	clear
	#Colocar condições para nao sobreescrever caso ja exista
	baixar_fonte
	read -p "Deseja configurar os arquivos? (s/n)" configurar_arquivos
	if [[ "$configurar_arquivos" == "s" ]]
		then
		echo "Deseja ver as configuracoes atuais? (s/n)"
		read op_ver_info
		if [[ "$op_ver_info" == "s" ]]
			then
			obter_info
		else 
			:
		fi

		inserir_dados

		read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
	else
		read -p "Ja havia configurado antes? (s/n) " op_preparado
		if [[ "$op_preparado" = "s" ]] ; then
			FLAG_PREPARADO=0
			menu_inicial
		else 
			FLAG_PREPARADO=1
			menu_inicial
		fi
	fi
	FLAG_PREPARADO=0;
}

#Executa script de upgrade do basecode ajustando detalhes
upgrade() {
	if [[ "$FLAG_PREPARADO" = 0 ]]; then

		clear
		echo -e "\nUpgrade!\n"
		cd $EDIR_ROOT/custom
		
		echo -e "Transferindo domain_1 do cliente para edirectory/web/custom/ ..."
		tar zcf domains.tar.gz domain_*
		mv domains.tar.gz $EDIR11_ROOT/web/custom/
		cd $EDIR11_ROOT/web/custom/
		mv domain_1/ domain_1_bkp
		tar zxf domains.tar.gz
		echo -e "Transferencia ok!\n"

		cd $EDIR11_ROOT
		echo -e "\nAjustando permissoes..."
		find ./ -type d -exec chmod 755 {} \;
		find ./ -type f -exec chmod 644 {} \;
		echo -e "Permissoes ok! (755 diretorios, 644 arquivos)\n"
		echo -e "Deseja realmente rodar o script de upgrade?\n"
		echo -e "Tenha certeza de que:"
		echo -e "01 - Os arquivos da pasta edirectory estao configurados corretamente;"
		echo -e "02 - A pasta custom/domain_1 da versao antiga foi copiada para edirectory/web/custom;"
		echo -e "03 - As permissoes estao ok (755 para diretorios, 644 para arquivos);"
		read -p "Confirmar? (s/n)" rodar_upgrade
		if [[ "$rodar_upgrade" == s ]]; then
		        cd $EDIR11_ROOT
		        php app/console edirectory:upgrade --env=prod --force
		        php app/console edirectory:synchronize --env=prod --recreate-index --domain=$PROJDOMAIN1 -f $PROJDOMAIN1
		        #php app/console assets:install --symlink web --env=prod
		else
		        menu_inicial
		        exit;
		fi

	finalizar_upgrade
	read -p "Deseja voltar ao menu? (s/n)" voltar_menu
		if [[ "$voltar_menu" == "s" ]]; then
				menu_inicial
		fi
	else
		echo -e "\nVoce precisa preparar os arquivos e bancos primeiro!\n"
	fi
}

#Visualizar log
ver_log() {
	sed 's/^[ \t]*/        /' $LOGUP

	read -p "Deseja voltar ao menu? (s/n) " voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

#Finalizar o processo de upgrade, criando link simbolico para public_html
finalizar_upgrade() {
	cd $BEFORE_EDIR_ROOT
	echo -e "\nAtualizando public_html...\n"
	mv public_html/ public_html_old
	ln -s edirectory/web public_html
	echo -e "\nSucesso!\n"
	echo -e "!\nVisite $PROJDOMAIN1 e confira o resultado!"
}

#####################################################################
#	Funcoes para Menu
####################################################################

menu_upgrade() {
	clear
	echo -e "\nMENU DE UPGRADE\n"
	echo -e "[1] - Ver informacoes atuais;"
	echo -e "[2] - Mudar informacoes;"
	echo -e "[3] - Preparar ambiente v11;"
	echo -e "[4] - Iniciar Upgrade;"
	echo -e "\n[0] - Voltar"

	read op_upgrade
		if [[ "$op_upgrade"   == 1 ]]
			then
			obter_info
		elif [[ "$op_upgrade" == 2 ]] 
			then
			inserir_dados
		elif [[ "$op_upgrade" == 3 ]] 
			then
			instalar
		elif [[ "$op_upgrade" == 4 ]] 
			then
			upgrade
		elif [[ "$op_upgrade" == 0 ]] 
			then
			menu_inicial
		else
			menu_upgrade
		fi

	echo ""
	read -p "Deseja voltar ao menu? (s/n) " voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

menu_log() {
	clear
	echo -e "\nRELATORIO\n"
	echo -e "[1] - Ver informacoes sobre arquivos;"
	echo -e "[2] - Ver informacoes sobre banco de dados;"
	echo -e "[3] - Ver relatorio completo;"
	echo -e "\n[0] - Voltar"

	read op_log
		if [[ "$op_log" ==  1 ]]
			then
			ver_log
		elif [[ "$op_log" == 0 ]] 
			then
			menu_inicial
		else
			menu_log
		fi

	echo ""
	read -p "Deseja voltar ao menu? (s/n) " voltar_menu
		if [[ "$voltar_menu" == "s" ]]
			then
			menu_inicial
		fi
}

menu_bkp() {
	clear
	echo -e "\nMenu de Backup\n"
	echo -e "[1] - Fazer Backup dos Bancos de Dados"
	echo -e "[2] - Fazer Backup dos arquivos da v10.4"
	echo -e "\n[0] - Voltar"


	read op_menu_bkp
	if [[ "$op_menu_bkp"   == 1 ]]
		then
		bkp_bds
	elif [[ "$op_menu_bkp"   == 2 ]]
		then
		bkp_files
	fi
}

menu_inicial() {
	clear
	echo -e "\n\tUTILITARIO V11\n"

	echo -e "[1] - Bloquear Site Manager"
	echo -e "[2] - Desloquear Site Manager\n"

	echo -e "[3] - Ver informacoes em vigor"			
	echo -e "[4] - Mudar informacoes\n"

	echo -e "[5] - Fazer Backup dos Bancos de Dados"	#Precida de info do MySQL
	echo -e "[6] - Fazer Backup do Projeto\n"				

	echo -e "[7] - Preparar ambiente v11"				#Colocar flags 1 ou 0
	echo -e "[8] - Iniciar Upgrade\n"					#Checar se esta preparado

	echo -e "[9] - Relatorio"
	
	echo -e "\n[0] - Sair"

	read op_menu_inicial
	if [[ "$op_menu_inicial"   == 1 ]]
		then
		block_sitemgr
	elif [[ "$op_menu_inicial" == 2 ]]
		then
		unblock_sitemgr
	elif [[ "$op_menu_inicial" == 3 ]] 
		then
		obter_info
	elif [[ "$op_menu_inicial" == 4 ]] 
		then
		inserir_dados
	elif [[ "$op_menu_inicial" == 5 ]] 
		then
		  bkp_bds
	elif [[ "$op_menu_inicial" == 6 ]] 
		then
		  bkp_files
	elif [[ "$op_menu_inicial" == 7 ]] 
		then
		  instalar
	elif [[ "$op_menu_inicial" == 8 ]] 
		then
		  upgrade
	elif [[ "$op_menu_inicial" == 9 ]] 
		then
		  ver_log
	elif [[ "$op_menu_inicial" == 0 ]] 
		then
		exit
	fi
}

#####################################################################
#	Ainda nao testadas
#####################################################################

loop_bkp_varios_dominios() {
	#Temporario, so pra manter anotacao
	DB_USER='craftbre_edir'
	DB_PASS='Edir_2014!'
	DB_MAIN_NAME='craftbre_main'
	PROJNAME='craftbre'

	mysql -u$DB_USER -p$DB_PASS -e "show databases;"

	for (( i=1; i<13; i++ ))
	do
	#echo $i
	mysqldump -C -u$DB_USER -p$DB_PASS craftbre_domain$i > bkp.craftbre_domain$i.sql
	done
}

quando_tiver_addons() {

	cd $EDIR11_ROOT/app/config/domains
	read -p "Quantos add-ons? " quantidade
	for (( i=0; i<quantidade; i++ ))
	do
		read -p "Informe o dominio: " DOMINIO
		cp domain.configs.yml.sample $DOMINIO.configs.yml
		cp domain.payment.yml.sample $DOMINIO.payment.yml
		cp domain.route.yml.sample $DOMINIO.route.yml
		echo "[+] - Arquivos criados para $DOMINIO"
	done
	echo -e "\nCONCLUIDO!\n"

	#for (( i=0; i<quantidade; i++ ))
	#... continuar, lembrando de adicionar tambem as entradas em web/custom/domain/domain.inc.php
}

inserir_cron() {
	cat /dev/null > crons.sql
	echo -e "Gerar lista de crons...\n"
	echo -e "Quantos dominios? "
	read qnt_dominio
	echo -e "INSERT INTO \`Control_Cron\` (\`domain_id\`, \`running\`, \`last_run_date\`, \`type\`) VALUES\n" >> crons.sql
	for (( i=1; i<=qnt_dominio; i++ ))
	do
	echo "
	($i, 'N', '0000-00-00 00:00:00', 'count_article_category'),
	($i, 'N', '0000-00-00 00:00:00', 'count_classified_category'),
	($i, 'N', '0000-00-00 00:00:00', 'count_event_category'),
	($i, 'N', '0000-00-00 00:00:00', 'count_listing_category'),
	($i, 'N', '0000-00-00 00:00:00', 'count_post_tag'),
	($i, 'N', '0000-00-00 00:00:00', 'daily_maintenance'),
	($i, 'N', '0000-00-00 00:00:00', 'randomizer'),
	($i, 'N', '0000-00-00 00:00:00', 'renewal_reminder'),
	($i, 'N', '0000-00-00 00:00:00', 'report_rollup'),
	($i, 'N', '0000-00-00 00:00:00', 'sitemap'),
	($i, 'N', '0000-00-00 00:00:00', 'statisticreport'),
	($i, 'N', '0000-00-00 00:00:00', 'location_update'),
	($i, 'N', '0000-00-00 00:00:00', 'prepare_import_events'),
	($i, 'N', '0000-00-00 00:00:00', 'email_traffic'),
	($i, 'N', '0000-00-00 00:00:00', 'rollback_import'),
	($i, 'N', '0000-00-00 00:00:00', 'prepare_import'),
	($i, 'N', '0000-00-00 00:00:00', 'rollback_import_events'),
	($i, 'N', '0000-00-00 00:00:00', 'count_locations');
	" >> crons.sql
	echo "[!] Adicionado com sucesso no dominio $i !"
	done
	echo -e "\nConcluido!"
	echo "Deseja visualizar? (s/n)"
	read ver
	if [[ "$ver" == "s" ]]
	then
	        less crons.sql
	else
	        :
	fi
}

msg_sitemgr() {
	if [[ "$sitemgr_status" = 0 ]] 
		then
		clear 
		echo -e "\n=========================================="
		echo -e "\nSite Manager Desbloqueado com sucesso!!!"
		echo -e "==========================================\n"
		sleep 2
	elif [[ "$sitemgr_status" = 1 ]]
		then
		clear 
		echo -e "\n=========================================="
		echo -e "\nSite Manager Bloqueado com sucesso!!!"
		echo -e "==========================================\n"
		sleep 2
	fi
}

#####################################################################

main
menu_inicial

#Guardar para funcao de operacoes em banco de dados
#
#Mudar 
#mysql -u$DB_USER -p$DB_PASS -e "use $DB_MAIN_NAME; update Domain set name='$DB_DOMAIN1', database_username='$DB_USER', database_password='$DB_PASS', database_name='$DB_DOMAIN1', url='$DOMAIN1' where id='1'"
#UPDATE `$DB_MAIN_NAME`.`SMAccount` SET `username` = 'arcalogin@arcasolutions.com', `password` = MD5( 'arca*login!' ) WHERE `SMAccount`.`id` =1;
